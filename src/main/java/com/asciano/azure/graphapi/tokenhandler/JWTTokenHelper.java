package com.asciano.azure.graphapi.tokenhandler;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.asciano.azure.graphapi.exceptions.GraphAPIException;
import com.asciano.azure.graphapi.services.AppParameter;
import org.json.*;

/**
 * Facilitates minting a test token.
 * 
 * @author Microsoft Corp
 *
 */
public class JWTTokenHelper {

    protected static final Log logger = LogFactory.getLog(JWTTokenHelper.class);

    /**
     * Grant type claim
     */
    private static final String claimTypeGrantType = "grant_type";

    /**
     * Assertion Claim.
     */
    // private static final String claimTypeAssertion = "assertion";

    /**
     * Resource Claim.
     */
    private static final String claimTypeResource = "resource";

    /**
     * Prefix for bearer tokens.
     */
    private static final String bearerTokenPrefix = "Bearer ";

    /**
     * Prefix for client id
     */
    private static final String clientIdPrefix = "client_id";

    /**
     * Prefix for client secret
     */
    private static final String clientSecretPrefix = "client_secret";

    /**
     * Get an access token from EVO STS
     * 
     * @param evoStsUrl
     *            EVO STS Url.
     * @param assertion
     *            Assertion Token.
     * @param resource
     *            ExpiresIn name.
     * @return The OAuth access token.
     * @throws GraphAPIException
     *             If the operation can not be completed successfully.
     */
    public static String getOAuthAccessTokenFromEvoSTS(String evoStsUrl, String clientId,String clientSecret, String resource) throws GraphAPIException {
        
        String accessToken = "";
                
        URL url = null;
        
        String data = null;
        
        try {
            data = URLEncoder.encode(JWTTokenHelper.claimTypeGrantType, "UTF-8") + "=" + URLEncoder.encode("client_credentials", "UTF-8");
            data += "&" + URLEncoder.encode(JWTTokenHelper.claimTypeResource, "UTF-8") + "=" + URLEncoder.encode(resource, "UTF-8");
            data += "&" + URLEncoder.encode(JWTTokenHelper.clientIdPrefix, "UTF-8") + "=" + URLEncoder.encode(clientId, "UTF-8");
            data += "&" + URLEncoder.encode(JWTTokenHelper.clientSecretPrefix, "UTF-8") + "=" + URLEncoder.encode(clientSecret, "UTF-8");
            
            url = new URL(evoStsUrl);
            
            URLConnection conn = url.openConnection();
            
            conn.setDoOutput(true);
            
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();
            
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            
            String line, response = "";
            
            while((line=rd.readLine()) != null){
                response += line;
            }
            
            wr.close();
            rd.close();
            
            logger.debug("Response from OAuth: "+response);
            
            accessToken = (new JSONObject(response)).optString("access_token");
            
            logger.debug("Graph Api Access Token="+accessToken);
            
            return String.format("%s%s", JWTTokenHelper.bearerTokenPrefix, accessToken);
            
        } catch (Exception e2) {
            throw new GraphAPIException(AppParameter.ErrorGeneratingToken, AppParameter.ErrorGeneratingTokenMessage, e2, null);
        }
    }
}
