package com.asciano.azure.graphapi;

import com.asciano.azure.graphapi.tokenhandler.TokenGenerator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.asciano.azure.graphapi.exceptions.GraphAPIException;
import com.asciano.azure.graphapi.services.AppParameter;
import com.asciano.azure.graphapi.services.RestAPIUserServices;
import org.json.JSONObject;

public class GraphLookup {
    
    protected final Log logger = LogFactory.getLog(getClass());

    public JSONObject getUserAttributes(String apiVersion, String appPrincipalId,
                                        String symmetricKey, String tenantDomain, String upn, String authUrl) throws GraphAPIException, Exception {
        
        AppParameter.setDataContractVersion("api-version=" + apiVersion);
        AppParameter.setAppPrincipalId(appPrincipalId);
        AppParameter.setAcsPrincipalId("00000001-0000-0000-c000-000000000000");
        AppParameter.setTenantContextId(tenantDomain);
        AppParameter.setTenantDomainName(tenantDomain);
        AppParameter.setProtectedResourcePrincipalId("00000002-0000-0000-c000-000000000000");
        AppParameter.setProtectedResourceHostName("graph.windows.net");
        AppParameter.setRestServiceHost("graph.windows.net");
        AppParameter.setEvoStsUrl("https://login.windows.net/%s/oauth2/token?api-version=1.0");
        AppParameter.setSymmetricKey(symmetricKey);

        try {
            AppParameter.setAccessToken(
                TokenGenerator.generateEvoStsToken(
                    AppParameter.getTenantContextId(),
                    AppParameter.getAppPrincipalId(),
                    AppParameter.getSymmetricKey(),
                    AppParameter.getProtectedResourceHostName()
                )
            );

        } catch (GraphAPIException ex1) {
            logger.error("Cannot generate Access Token", ex1);;
            throw ex1;
        } catch (Exception ex2) {
            logger.error(ex2);
            throw ex2;
        }
        
        logger.info(String.format("apiVersion: %s, appPrincipalId: %s, symmetricKey: %s, tenantDomain: %s, upn: %s", 
                apiVersion, appPrincipalId, symmetricKey, tenantDomain, upn));

        try {
            JSONObject jsonObj = RestAPIUserServices.getUserExtensionAttribute(upn);
            return jsonObj;
        } catch (GraphAPIException e) {
            logger.error("User Credentials cannot be retrieved", e);
            throw e;
        }

    }
}
