/**
 * 
 */
package com.asciano.azure.graphapi.services;

import java.util.Iterator;

import com.asciano.azure.graphapi.exceptions.GraphAPIException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.json.*;



/**
 * This class provides all the read functionalities.
 * @author Microsoft Corp
 *
 */
public class RestAPIUserServices {
    
    protected final static Log logger = LogFactory.getLog(RestAPIUserServices.class);
    
    public static JSONObject getUserExtensionAttribute(String objectId) throws GraphAPIException {
        
        /**
         * Create the additional path and also we don't have any query option
         * for this operation. Invoke the handleRequest method and get the 
         * response in a String object.        
         */
        String response = HttpRequestHandler.handleRequest(
                String.format("/users/%s", objectId), 
                null);
        
        logger.debug("Response from Graph API User: "+response);
        
        /**
         * Get a JSONObject that would hold the attributes of the user.
         */        
        JSONObject userObject = JSONDataParser.parseJSonDataSingleObject(response);    
        
        try {
            userObject.put("extension_employeeId", getEmployeeInfo(userObject, "employeeID"));
        } catch (JSONException e) {
            logger.error("Unable to retrieve employeeID from Graph API response");
        }
        
        try {
            userObject.put("extension_employeeNumber", getEmployeeInfo(userObject, "employeeNumber"));
        } catch (JSONException e) {
            logger.error("Unable to retrieve employeeNumber from Graph API response");
        }

        return userObject;
    }    
    
    protected static String getEmployeeInfo(JSONObject userObject, String attributeSuffix) throws JSONException {
        for(Iterator<String> i = userObject.keys(); i.hasNext(); ) {
            String item = i.next();
            
            if (item.startsWith("extension") && item.endsWith(attributeSuffix))
            {
                logger.debug("Found "+attributeSuffix+": "+item);

                return userObject.getString(item);

            }
        }    
        
        return null;
    }
    
}
